const { Language } = require("../../models/languageModel");

exports.Home = async (req, res) => {
  try {
    const lang = await Language.find();
    if (!lang || lang === null) {
      return res.status(400).json({ msg: "No such language" });
    }
    console.log(lang)
    res.json({
      lng: {
        en: lang[0],
        // ru: lang[2],
      },
    });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};
