const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const cors = require("cors");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const router = express.Router();
require("dotenv").config();

const app = express();
app.use(helmet());

app.use(morgan("dev"));
const port = process.env.PORT || 9000;

app.use(bodyParser.json({ limit: "2mb" }));

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;

mongoose.connect(uri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});
const connection = mongoose.connection;
connection.once("open", () => {
  console.log("MongoDB database connection established successfully");
});

const allLanguages = require("./routes/language");

app.use("/", allLanguages);

module.exports = router;

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
