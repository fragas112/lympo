const mongoose = require("mongoose");

const Schema = mongoose.Schema;


const languageSchema = new Schema ({
    languageName: {
        type: String,
        required: true,
      },

      topNavigation: {
          workTime: {
            value: {
              type: String,
              require: true
            }
           
          }
      },
      clientPage : {
        link : {
          type: String,
        } ,
        pageFields : {
          title: {
            type: String,
        }

      }
    },
    aboutUs : {
      link : {
        type: String,
      } ,
      pageFields : {
        title: {
          type: String,
      }

    }
  },
  buyingOrder : {
    link : {
      type: String,
    } ,
    pageFields : {
      title: {
        type: String,
    }

  }
},

});
module.exports = {
    Language: mongoose.model("Language", languageSchema),
    languageSchema: languageSchema,
  };