import React from 'react';
import styles from '../../../styles/aboutLympo3rd.module.scss'
import { useSelector } from "react-redux";
import { StoreState } from '../../../store/store';
export default function Navigation() {
     //Language
 const language = useSelector((state: StoreState) => state.language);
 const lngValue = language?.lngValue;
 const stage = language?.stage
 //Words
 const title = language?.[lngValue]?.Main?.AboutLympo?.title;
 const text = language?.[lngValue]?.Main?.AboutLympo?.text;
 const text2 = language?.[lngValue]?.Main?.AboutLympo?.text2;

  return (
    <React.Fragment>
      <div className={styles.About}>
        <div className={styles.Wrapper}>
        <div className={styles.AboutText}>
        <h1>{title}</h1>
        <p>{text} </p>
          <br></br>
            <p>{text2}</p>
</div>
</div>
      </div>
    
    </React.Fragment>
  )
}
