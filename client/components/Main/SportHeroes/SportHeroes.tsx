import React from 'react';
import styles from '../../../styles/sportHeroes.module.scss'
import { useSelector } from "react-redux";
import { StoreState } from '../../../store/store';
import { Fade } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import { useMediaQuery } from 'react-responsive'
import "../../../styles/sportHeroes.module.scss"

export default function SportHeroes() {

  const isTablet = useMediaQuery({ query: '(max-width: 950px)' })
  const isPhone = useMediaQuery({ query: '(max-width: 850px)' })
 
  const style1: any = {
    width: isPhone ? "320px" : "370px",
  position: "absolute",
  top:"-320px",
  right: isTablet ? "48%" :"58%",
  zIndex: 3
  }
  const style2: any = {
    width: isPhone ? "320px" : "370px",
  height: "555px",
  position: "absolute",
  top:"-250px",
  left: isTablet ? "20%" :"38%",
  zIndex: 2
  }
  const style3: any = {
    width: isPhone ? "320px" : "370px",
  height: "555px",
  position: "absolute",
  top:"-320px",
  left:  isTablet ? "48%" :"58%",
  zIndex: 1
  }
  const properties = {
    autoplay: true,
    arrows: false,
    pauseOnHover:true
  };
 //Language
 const language = useSelector((state: StoreState) => state.language);
 const lngValue = language?.lngValue;
 
 //Words
 const title = language?.[lngValue]?.Main?.SportHeroes?.title;
 const text = language?.[lngValue]?.Main?.SportHeroes?.text;
 const subscribe = language?.[lngValue]?.Main?.SportHeroes?.subscribe;
 const email = language?.[lngValue]?.Main?.SportHeroes?.email;
 const wallet = language?.[lngValue]?.Main?.SportHeroes?.wallet;

  return (
    <React.Fragment>
      <div className={styles.Sport}>
          <img className={styles.corner} src="./big_gray_02.svg" alt="corner"/>
          <img className={styles.cornerMobile} src="./small_gray_02.svg" alt="cornerMobile"/>
          <img className={styles.corner2} src="./big_gray_03.svg" alt="corner2"/>
          <img className={styles.cornerMobile2} src="./small_gray_01.svg" alt="cornerMobile2"/>
        <div className={styles.SportBox}>
          <div className={styles.SportImages}>
            {/* <img src="/Cards.webp" alt="sportHeroesCards"/> */}
            <div className="slide-container" style={style1}>
                <Fade {...properties}>
                  <div className="each-fade">
                  <div className="image-container">
                      <img  src="/Lympo Warren Harvey Legendary.png" alt="Lympo Warren Harvey Legendary"/>
                    </div>
                  
                  
                  </div>
                  <div className="each-fade">
                  <div className="image-container">
                    <img   src="/Lympo Sakata Natsu Legendary.png" alt="Lympo Sakata Natsu Legendary"/>
                    </div>
                   
                  </div>
                  <div className="each-fade">
                  
                    <div className="image-container">
                      <img  src="/Lympo Trevor Schafer Legendary.png" alt="Lympo Trevor Schafer Legendary"/>
                    </div>
                  </div>
                
                </Fade>
            </div>
            <div className="slide-container" style={style2}>
                <Fade {...properties}>
                  <div className="each-fade">
                  <div className="image-container">
                      <img  src="/Lympo Trevor Schafer Epic.png" alt="Lympo Trevor Schafer Epic"/>
                    </div>
                  
                  
                  </div>
                  <div className="each-fade">
                  
                    <div className="image-container">
                      <img  src="/Lympo Warren Harvey Epic.png" alt="Lympo Warren Harvey Epic"/>
                    </div>
                  </div>
                  <div className="each-fade">
                  <div className="image-container">
                    <img   src="/Lympo Vincent Pinho Passos Epic.png" alt="Lympo Vincent Pinho Passos Epic"/>
                    </div>
                   
                  </div>
                
                </Fade>
            </div>
            <div className="slide-container" style={style3}>
                <Fade {...properties}>
                  <div className="each-fade">
                    <div className="image-container">
                    <img   src="/Lympo Sakata Natsu Rare.png" alt="Lympo Sakata Natsu Rare"/>
                    </div>
                  
                  </div>
                  <div className="each-fade">
                  <div className="image-container">
                      <img  src="/Lympo Trevor Schafer Rare.png" alt="Lympo Trevor Schafer Rare"/>
                    </div>
                    
                  </div>
                  <div className="each-fade">
                  <div className="image-container">
                      <img  src="/Lympo Holly O'Doherty Rare.png" alt="Lympo Holly O'Doherty Rare"/>
                    </div>
                   
                  </div>
                
                </Fade>
            </div>
           
           
          </div>
          <div className={styles.MobileCards} >
            <img className={styles.frame} src="/frames2.png" alt="sportHeroesCardsMobile"/>
            <div className={styles.mmaMobile}>
            <Fade {...properties}>
                  <div className="each-fade">
                  <div className="image-container">
                      <img  src="/Lympo Warren Harvey Legendary.png" alt="Lympo Warren Harvey Legendary"/>
                    </div>
                  
                  
                  </div>
                  <div className="each-fade">
                  <div className="image-container">
                    <img   src="/Lympo Sakata Natsu Legendary.png" alt="Lympo Sakata Natsu Legendary"/>
                    </div>
                   
                  </div>
                  <div className="each-fade">
                  
                    <div className="image-container">
                      <img  src="/Lympo Trevor Schafer Legendary.png" alt="Lympo Trevor Schafer Legendary"/>
                    </div>
                  </div>
                
                </Fade>
            </div>
            <div className={styles.basketballMobile}>
            <Fade {...properties}>
                  <div className="each-fade">
                  <div className="image-container">
                      <img  src="/Lympo Trevor Schafer Epic.png" alt="Lympo Trevor Schafer Epic"/>
                    </div>
                  
                  
                  </div>
                  <div className="each-fade">
                  
                    <div className="image-container">
                      <img  src="/Lympo Warren Harvey Epic.png" alt="Lympo Warren Harvey Epic"/>
                    </div>
                  </div>
                  <div className="each-fade">
                  <div className="image-container">
                    <img   src="/Lympo Vincent Pinho Passos Epic.png" alt="Lympo Vincent Pinho Passos Epic"/>
                    </div>
                   
                  </div>
                
                </Fade>
            </div>
                <div className={styles.footballMobile}>
                <Fade {...properties}>
                  <div className="each-fade">
                    <div className="image-container">
                    <img   src="/Lympo Sakata Natsu Rare.png" alt="Lympo Sakata Natsu Rare"/>
                    </div>
                  
                  </div>
                  <div className="each-fade">
                  <div className="image-container">
                      <img  src="/Lympo Trevor Schafer Rare.png" alt="Lympo Trevor Schafer Rare"/>
                    </div>
                    
                  </div>
                  <div className="each-fade">
                  <div className="image-container">
                      <img  src="/Lympo Holly O'Doherty Rare.png" alt="Lympo Holly O'Doherty Rare"/>
                    </div>
                   
                  </div>
                
                </Fade>
                </div>
    
          </div>
          <div className={styles.Form}>
            <div className={styles.formWrapper}>
            <h4>Some awesome NFT sporting heroes!</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In accumsan neque sed felis vulputate rhoncus. Duis viverra erat at scelerisque pretium. Nunc mattis lacus eget placerat tempor. In rutrum velit vel ex ultricies luctus. Donec a purus ornare, ullamcorper turpis et, hendrerit elit. Proin euismod nunc eget libero convallis finibus.Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <div className="ml-form-embed"
        id={styles.form}
      data-account="1121894:y0z8g8r7u8"
      data-form="3612595:z1g9s4">
      </div> 
            </div>
         
          </div>
         
        </div>
     
      </div>
    
    </React.Fragment>
  )
}
