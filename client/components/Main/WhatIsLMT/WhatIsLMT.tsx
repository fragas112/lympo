import React from 'react';
import styles from '../../../styles/lmt.module.scss'
import { useSelector } from "react-redux";
import { StoreState } from '../../../store/store';
export default function Navigation() {
       //Language
 const language = useSelector((state: StoreState) => state.language);
 const lngValue = language?.lngValue;
 const stage = language?.stage
 //Words
 const title = language?.[lngValue]?.Main?.LMT?.title;
 const text = language?.[lngValue]?.Main?.LMT?.text;
 const text2 = language?.[lngValue]?.Main?.LMT?.text2;
  return (
    <React.Fragment>
      <div className={styles.LMT}>

      {/* IF FIRST STAGE IS TRUE */}
      {stage === "1st" ? <img className={styles.cornerMobile} src="./small_white_02.svg" alt="corner"/> : null}
      {/* IF SECOND STAGE IS TRUE */}
      {stage === "1st" ? null :<img className={styles.corner} src="./big_white_04.svg" alt="corner"/>}
      {stage === "1st" ? null :<img className={styles.cornerMobile2} src="./small_white_04.svg" alt="corner"/>}
     
        <div className={styles.TestBox}>
        <div className={styles.LMTBox}>
          <div className={styles.LMTLeft}><img src="/Coin.webp" alt="LMTcoin"/></div>
          <div className={styles.LMTRight}>
            <h1>{title}</h1>
          <img className={styles.mobileImg} src="/Coin.webp" alt="LMTcoin"/>
            <p>{text}</p>
            <br></br>
            <p>{text2}</p>
          </div>
        </div>
        </div>
      </div>
    
    </React.Fragment>
  )
}
