import React from 'react';
import VideoSection from '../Main/VideoSection/VideoSection';
import AboutLympo from '../Main/AboutLympo/AboutLympo';
import LMT from '../Main/WhatIsLMT/WhatIsLMT';
import SportHeroes from '../Main/SportHeroes/SportHeroes';
import TokenSales from '../Main/TokenSales/TokenSales';
import styles from '../../styles/main.module.scss';
import ExchangeForm from '../HeroBanner2/ExchangeForm/ExchangeForm';
import Footer from '../Footer/Footer';
import { StoreState } from '../../store/store';
import { useSelector } from "react-redux";
export default function Home() {
  const language = useSelector((state: StoreState) => state.language);
  const stage = language?.stage
  return (
    <React.Fragment>
      <div className={styles.Main}>
      {stage === "1st" ? <VideoSection /> : stage === "2nd" ? <ExchangeForm/>: null}
        <AboutLympo />
        <LMT />
        <SportHeroes />
        <TokenSales />
        <Footer/>
      </div>
    </React.Fragment>
  )
}
