import React from 'react';
import styles from '../../../styles/tokenSales.module.scss'
import { useSelector } from "react-redux";
import { StoreState } from '../../../store/store';
export default function TokenSales() {
     //Language
 const language = useSelector((state: StoreState) => state.language);
 const lngValue = language?.lngValue;
 //Words
 const title = language?.[lngValue]?.Main?.TokenSales?.title;
 const text = language?.[lngValue]?.Main?.TokenSales?.text;
 const title2 = language?.[lngValue]?.Main?.TokenSales?.title2;
 const text2 = language?.[lngValue]?.Main?.TokenSales?.text2;
  return (
    <React.Fragment>
      <div className={styles.TokenSales}>
        <div className={styles.Wrapper}>
        <div className={styles.TokenBox}>
          <h1>{title}</h1>
          <p>{text}</p>
          <br></br>
          <p>{text2}</p>
          <img className={styles.PcTable} src="/table.webp" alt="LTMCoinTable" />
          <img className={styles.TableMobile} src="/TableMobile.webp" alt="LTMCoinTableMobile" />
        </div>
        <hr />
        <div className={styles.LymTokens}>
          <h1>{title2}</h1>
          <div className={styles.divBox}>
              <a href="https://www.huobi.com" target="_blank"><img  src="/huobi.webp" alt="Huobi"/></a>
              <a href="https://www.gate.io" target="_blank"><img src="/gate.webp" alt="Gate"/></a>
              <a href="https://uniswap.org" target="_blank"><img src="/uniswap.webp" alt="Uniswap"/></a>
              <a href="https://www.kucoin.com" target="_blank"><img src="/kucoin.webp" alt="KuCoin"/></a>
              <a href="https://www.gopax.com/" target="_blank"><img src="/gopax.webp" alt="Gopax"/></a>
              <a href="https://www.bitfinex.com" target="_blank"><img src="/bitfinex.webp" alt="Bitfinex"/></a>
          </div>
        </div>
        </div>
      </div>
    
    </React.Fragment>
  )
}
