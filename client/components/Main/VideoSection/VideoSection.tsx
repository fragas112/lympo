import React, { useState } from 'react';
import styles from '../../../styles/videoSection.module.scss'

export default function Navigation() {
  const [trigger, setTrigger] = useState<boolean>(false)
  const changeToVideo = () => {
    setTrigger(true)
  }
  let videoImage = <div><img className={styles.videoBackground} src="/Video.webp"></img> <img onClick={() => changeToVideo()} className={styles.Playbutton} src="/PlayButton.webp" alt=""/></div>;
  let video = <iframe  src="https://www.youtube.com/embed/5qap5aO4i9A" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
  return (
    <React.Fragment>
      <div className={styles.VideoSection}>
        {!trigger
          ?videoImage
          : video
        }
        <img className={styles.corner} src="./big_white_03.svg" alt="corner"/>
        <img className={styles.cornerMobile} src="./small_gray_04.svg" alt="cornerMobile"/>
        <img className={styles.cornerMobile2} src="./small_white_01.svg" alt="cornerMobile"/>
      </div>
    
    </React.Fragment>
  )
}
