import React from 'react'
import styles from '../../styles/navigation.module.scss'


export default function Navigation() {
  return (
    <React.Fragment>

      <div className={styles.Navigation}>
        <div className={styles.LogoBox}>
        <a href="/"><img src="/LogoLympo.webp" alt="LympoLogo"/></a>
        </div>
        <div className={styles.Social}>
        <a href=""><img src="/Reddit.webp" alt="LympoReddit"/></a>
        <a href=""><img src="/Twitter.webp"alt="LympoTwitter"/></a>
        <a href=""><img src="/Telegram.webp"alt="LympoTelegram"/></a>
        
      </div>
      </div>
    
    </React.Fragment>
  )
}
