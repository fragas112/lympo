import React from 'react'
import styles from '../../../styles/subscribe2.module.scss'
import { useSelector } from "react-redux";
import { StoreState } from '../../../store/store';
export default function Subscribe() {
     //Language
 const language = useSelector((state: StoreState) => state.language);



  return (
    <React.Fragment>

      <div className={styles.Subscribe}>
        <div className={styles.ExtraBorder}>
          <span className={styles.LeftRectangle}><img src="/RectangleYellow.webp"alt="LympoContactForm"></img></span>
          <span className={styles.RightRectangle} ><img src="/Rectangle.png"alt="LympoContactForm"></img></span>
        <div className={styles.SubscribeBox}>
          <div className={styles.FormRight}>
          <video className={styles.mma} width="320px"  autoPlay muted loop>
            <source src="Kickboxer.mp4" type="video/mp4"/>
          </video>
        <img src="./frames.png" alt="cardFrame" width="320px"/>
            </div>
          <div className={styles.TokensSold}>
            <h2>Tokens sold:</h2>
            <h3 className={styles.Light}><img  src="/lmticon.webp" alt="lmtIcon"/>160 000 000.671</h3>
            <h3 className={styles.Dark}><img  src="/lmtDark.png" alt="lmtIcon"/>160 000 000.671</h3>
            <h3 className={styles.Light}><img src="/lymIcon.webp" alt="lymIcon"/>80 000 000.671</h3>
            <h3 className={styles.Dark} ><img src="/lymDark.png" alt="lmtIcon"/>160 000 000.671</h3>
          </div>                           
        </div>
        </div>

       
      </div>
    </React.Fragment>
  )
}
