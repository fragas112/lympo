import React from 'react'
import styles from '../../../styles/exchangeForm.module.scss'

export default function Subscribe() {

  return (
    <React.Fragment>
    <div className={styles.ExchangeForm}>
    <img className={styles.corner} src="./big_white_01.svg" alt="corner"/>
    <img className={styles.cornerMobile} src="./small_white_01.svg" alt="cornerMobile"/>
        <div className={styles.Wrapper}>
        <img className={styles.webImg} src="/formBack.webp" alt="AboutLympo"/>
        <div className={styles.FormBox}>
        <div className={styles.form}>
         <h2>Exchange tokens</h2>
       <div className={styles.Balance}>Balance:<h3><img src="/lymForm.webp"/>0,0000 Lym</h3></div>
       <input type="text" placeholder="Enter token amount"/>
       <h4>Add: <p>10 000</p> <p>50 000</p> <p>100 000</p> <p>MAX</p></h4>
       <hr/>
       <div className={styles.Balance}>Balance:<h3><img src="/lmtForm.webp"/>0,0000 Lmt</h3></div>
       <input type="text" placeholder="LMT amount"/>
       <button>Exchange</button>
       <p>You will receive 50% of your exchanged tokens right away. Remaining 50% will be transfered to you on a weekly basis 10% per week</p>
         </div>
        </div>
    
        </div>
    
    </div>
    </React.Fragment>
  )
}
