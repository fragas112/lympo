import React from 'react'
import styles from '../../../styles/textContent2.module.scss'
import { useSelector } from "react-redux";
import { StoreState } from '../../../store/store';
export default function TextContent() {
   //Language
 const language = useSelector((state: StoreState) => state.language);
 const lngValue = language?.lngValue;
 //Words
 const title = language?.[lngValue]?.heroBanner?.textContent?.title;
 const content = language?.[lngValue]?.heroBanner?.textContent?.content;

  return (
    <React.Fragment>

      <div className={styles.TextContent}>
    
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In accumsan neque sed felis vulputate rhoncus. Duis viverra erat at scelerisque pretium. Nunc mattis lacus eget placerat tempor. In rutrum velit vel ex ultricies luctus. Donec a purus ornare, ullamcorper turpis et.</p>
        <button><img src="/connect.webp"></img>Connect to meta mask</button>
        </div>
    </React.Fragment>
  )
}
