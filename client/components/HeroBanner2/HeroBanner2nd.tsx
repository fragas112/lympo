import React from 'react';
import Navigation from '../Navigation/navigation';
import styles from '../../styles/heroBanner2.module.scss';
import CountDown from './CountDown/CountDown';
import TextContent from '../HeroBanner2/TextContent/TextContent';
import Subscribe from '../HeroBanner2/Subscribe/Subscribe';

export default function HeroBanner() {
  return (
    <React.Fragment>

      <div className={styles.HeroBanner}>
      <img className={styles.corner} src="./big_gray_01.svg" alt="corner"/>
      <img className={styles.cornerMobile} src="./small_gray_01.svg" alt="corner"/>
        <div className={styles.Wrapper}>
        <Navigation/>
        <CountDown />
        <TextContent />
        <Subscribe/>
        </div>
      </div>
    </React.Fragment>
  )
}
