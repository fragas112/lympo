import React from 'react';
import styles from '../../styles/footer.module.scss'
import { useSelector } from "react-redux";
import { StoreState } from '../../store/store';
import FormFooter from '../Forms/footerForm';
export default function TokenSales() {
       //Language
 const language = useSelector((state: StoreState) => state.language);
 const lngValue = language?.lngValue;
 //Words
 const title = language?.[lngValue]?.Footer?.title;
 const title2 = language?.[lngValue]?.Footer?.title2;
 const button = language?.[lngValue]?.Footer?.button;
 const text = language?.[lngValue]?.Footer?.text
 const email = language?.[lngValue]?.Footer?.email
 const wallet = language?.[lngValue]?.Footer?.wallet
  return (
  
    <React.Fragment>
      <div className={styles.Footer}>
        <img className={styles.corner} src="./big_gray_04.svg" alt="corner"/>
        <img className={styles.cornerMobile} src="./small_gray_04.svg" alt="cornerMobile"/>
        <div className={styles.Wrapper}>
        <h1>
          {title}
      </h1>

          <div className={styles.Form}>
          <div className="ml-form-embed"
        id="formFooter"
      data-account="1121894:y0z8g8r7u8"
      data-form="3612595:z1g9s4">
      </div> 
          </div>
        <h2>{title2}</h2>
        <div className={styles.Socials}>
        <a href=""><img src="/Reddit.webp" alt="LympoReddit"/></a>
        <a href=""><img src="/Twitter.webp" alt="LympoTwitter"/></a>
        <a href=""><img src="/Telegram.webp" alt="LympoTelegram"/></a>
        </div>
          <p>{text}</p>
          </div>
      </div>
    
    </React.Fragment>
  )
} 
