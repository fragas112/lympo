import React, { useState } from "react";
import Navigation from "../Navigation/navigation";
import styles from "../../styles/heroBanner3.module.scss";

export default function HeroBanner() {
  const [show, setShow] = useState(false);
  const toggleClaim = () => {
    if (!show) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  const weMadeIt = (
    <div className={styles.Top}>
      <h1>We made it!</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In accumsan
        neque sed felis vulputate rhoncus. Duis viverra erat at scelerisque
        pretium. Nunc mattis lacus eget placerat tempor. In rutrum velit vel ex
        ultricies luctus. Donec a purus ornare, ullamcorper turpis et.
      </p>
      <button onClick={() => toggleClaim()}>
        <img src="/connect.webp"></img>Connect to meta mask
      </button>
    </div>
  );

  const claimForm = (
    <div className={styles.ClaimTokens}>
      <h2>Claim Tokens</h2>
      <form action="">
        <div className={styles.FormWrapper}>
          <h3>Your balance</h3>
          <div className={styles.TopForm}>
            <div className={styles.tokenBalance}>
              <div>
                <img src="./lymDark.png" alt="lymToken" />
                <p>9954812</p>
              </div>
            </div>
            <div className={styles.tokenBalance}>
              <div>
                <img src="./lmtDark.png" alt="lmtToken" />
                <p>9954812</p>
              </div>
            </div>
          </div>
          <img
            className={styles.ClaimLine}
            src="./claimLine.png"
            alt="claimTokensLine"
          />
          <div className={styles.MiddleForm}>
            <div className={styles.LymTokens}>
              <img src="./Loading.png" alt="loading" />
              <div>
                <h4>Time remaining:</h4>
                <p>34 days</p>
              </div>
            </div>
            <div className={styles.LmtTokens}>
              <img src="./Loading.png" alt="loading" />
              <div>
                <h4>Remaining tokens:</h4>
                <div className={styles.Remaining}>
                  <img src="./lmtForm.webp" alt="lmtIcon" />
                  <span>9954812 Lmt</span>
                </div>
              </div>
            </div>
          </div>
          <button>
            <p> Claim</p>
            <div className={styles.lmtBtn}>
              <img src="./lmtFormWhite.png" alt="lmtFormWhite" />
              <span>10000 Lmt</span>
            </div>
          </button>
        </div>
      </form>
    </div>
  );

  return (
    <React.Fragment>
      <div className={styles.HeroBanner}>
        <img className={styles.corner} src="./big_white_01.svg" alt="corner" />
        <img
          className={styles.cornerMobile}
          src="./small_white_01.svg"
          alt="corner"
        />
        <div className={styles.Wrapper}>
          <Navigation />
          <div className={styles.contentBox}>
            <div className={styles.LeftSide}>
              {!show ? weMadeIt : claimForm}

              <div
                className={styles.Bottom}
                style={!show ? { marginBottom: "75px" } : null}
              >
                <img className={styles.line} src="./Line.png" alt="Line" />
                <h2>Tokens sold:</h2>
                <div className={styles.Tokens}>
                  <h3>
                    <img src="/lmticon.webp" alt="lmtIcon" />
                    160 000 000.671
                  </h3>
                  <h3>
                    <img src="/lymIcon.webp" alt="lymIcon" />
                    80 000 000.671
                  </h3>
                </div>
              </div>
            </div>
            <div className={styles.RightSide}>
              <video className={styles.football} autoPlay muted loop>
                <source src="Tennis.mp4" type="video/mp4" />
              </video>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
