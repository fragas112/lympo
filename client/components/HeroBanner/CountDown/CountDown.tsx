import React from 'react'
import Countdown from 'react-countdown';
import styles from '../../../styles/countDown.module.scss'
import { useSelector } from "react-redux";
import { useMediaQuery } from 'react-responsive'
import { StoreState } from '../../../store/store';
import FlipCountdown from '../../FlipCountdown/flipcountdown';

export default function CountDown() {
  const isMobile = useMediaQuery({ query: '(max-width: 650px)' })
  let year = new Date().getFullYear();
      var t = +new Date(`04/06/${year}`) - +new Date();
  // Random component
const Completionist = () => <span>You are good to go!</span>;
 //Language
 const language = useSelector((state: StoreState) => state.language);
  const lngValue = language?.lngValue;
  //Words
  const countDownDays = language?.[lngValue]?.heroBanner?.countDown?.days;
  const countDownHours = language?.[lngValue]?.heroBanner?.countDown?.Hours;
  const countDownMinutes = language?.[lngValue]?.heroBanner?.countDown?.Minutes;
  const countDownSeconds = language?.[lngValue]?.heroBanner?.countDown?.Seconds;
// Renderer callback with condition
  const renderer = ({ days, hours, minutes, seconds, completed }: any,) => {
    let formattedDays = days.toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    })
    let formattedHours = hours.toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    })
    let formattedMinutes = minutes.toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    })
    let formattedSeconds = seconds.toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    })
  if (completed) {
    // Render a completed state
    return <Completionist />;
  } else {
    // Render a countdown
    return <div className={styles.CountDown}>
      <div className={styles.timer}>
        <p>{countDownDays}</p>
        <time>{formattedDays}</time>
      </div>
      <div className={styles.timer}>
        <p>{countDownHours}</p>
        <time>{formattedHours}</time>
      </div>
      <div className={styles.timer}>
        <p>{countDownMinutes}</p>
        <time>{formattedMinutes}</time>
      </div>
      <div className={styles.timer}>
        <p>{countDownSeconds}</p>
        <time>{formattedSeconds}</time>
      </div>
      
    </div>;


  }

 
};
  return (
    <React.Fragment>

      <div className={styles.CountDownContent} id="Counter">
        {!isMobile ? <FlipCountdown/> : <Countdown date={Date.now() + t} renderer={renderer} />}
      </div>
    </React.Fragment>
  )
}
