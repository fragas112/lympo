import React from 'react'
import styles from '../../../styles/subscribe.module.scss'
import '../../../styles/subscribe.module.scss'
import { useSelector } from "react-redux";
import { StoreState } from '../../../store/store';



export default function Subscribe() {

     //Language
 const language = useSelector((state: StoreState) => state.language);
 const lngValue = language?.lngValue;
 //Words
 const title = language?.[lngValue]?.heroBanner?.subscribe?.title;
 const text = language?.[lngValue]?.heroBanner?.subscribe?.text;
 const email = language?.[lngValue]?.heroBanner?.subscribe?.inputEmail;
 const wallet = language?.[lngValue]?.heroBanner?.subscribe?.inputWallet;
 const subscribe = language?.[lngValue]?.heroBanner?.subscribe?.button;

  return (
    <React.Fragment>

      <div className={styles.Subscribe}>
         <img className={styles.corner} src="/small_gray_01.svg"alt="corner"></img>
        <div className={styles.ExtraBorder}>
          <span><img src="/RectangleYellow.webp"alt="LympoContactForm"></img></span>
        <div className={styles.SubscribeBox}>
          <div className={styles.formContent}>
          <h4>Get the news, be the 1st in line!</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In accumsan neque sed felis vulputate rhoncus. Duis viverra erat at scelerisque pretium.</p>
        <div className="ml-form-embed"
        id={styles.form}
      data-account="1121894:y0z8g8r7u8"
      data-form="3612595:z1g9s4">
      </div> 
          </div>
        
 
        <div className={styles.videoCard}>
        <video className={styles.football} autoPlay muted loop>
            <source src="basketball.mp4" type="video/mp4"/>
          </video>
       
        </div>
        </div>
        </div>
        <div className={styles.videoCardMobile}>
        <video className={styles.football} autoPlay muted loop>
            <source src="basketball.mp4" type="video/mp4"/>
          </video>
      
        </div>
        <div className={styles.MobileBox}>
        <h4>Get the news, be the 1st in line!</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In accumsan neque sed felis vulputate rhoncus. Duis viverra erat at scelerisque pretium.</p>
        <div className="ml-form-embed"
        id={styles.formMobile}
      data-account="1121894:y0z8g8r7u8"
      data-form="3612595:z1g9s4">
      </div> 

        </div>
    
      </div>
    </React.Fragment>
  )
}
