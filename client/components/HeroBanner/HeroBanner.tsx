import React from 'react';
import Navigation from '../Navigation/navigation';
import styles from '../../styles/heroBanner.module.scss';
import CountDown from './CountDown/CountDown';
import TextContent from '../HeroBanner/TextContent/TextContent';
import Subscribe from '../HeroBanner/Subscribe/Subscribe';
export default function HeroBanner() {
  return (
    <React.Fragment>

      <div className={styles.HeroBanner}>
        <Navigation/>
        <CountDown />
        <TextContent />
        <Subscribe/>
      </div>
    </React.Fragment>
  )
}
