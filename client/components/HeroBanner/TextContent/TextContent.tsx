import React from 'react'
import styles from '../../../styles/textContent.module.scss'
import { useSelector } from "react-redux";
import { StoreState } from '../../../store/store';
export default function TextContent() {
   //Language
 const language = useSelector((state: StoreState) => state.language);
 const lngValue = language?.lngValue;
 //Words
 const title = language?.[lngValue]?.heroBanner?.textContent?.title;
 const content = language?.[lngValue]?.heroBanner?.textContent?.content;

  return (
    <React.Fragment>

      <div className={styles.TextContent}>
        <h1>{title}</h1>
        <p>{ content}</p>
        </div>
    </React.Fragment>
  )
}
