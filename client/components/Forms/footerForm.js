import React from 'react'
import styles from '../../styles/footer.module.scss'
export default function Form() {

  
  return (
    <React.Fragment>
<div id="mlb2-3612595" >
  <div>
    <div>
      <div>
        <form  action="https://static.mailerlite.com/webforms/submit/z1g9s4" data-code="z1g9s4" method="post" target="_blank">
          <div  className={styles.Inputs}>
          <input aria-label="email" aria-required="true" type="email" className="form-control" data-inputmask="" name="fields[email]" placeholder="Email address" autoComplete="email"/>
          <input aria-label="name" aria-required="true" type="text" className="form-control" data-inputmask="" name="fields[name]" placeholder="ERC20 wallet address" autoComplete="name"/>
          </div>
          <input type="hidden" name="ml-submit" value="1"/>
          <div>
            <button type="submit" className="primary">SUBSCRIBE</button>
          </div>
          <input type="hidden" name="anticsrf" value="true"/>
        </form>
      </div>
      
      {/* <div >
        <div >
          <h4>Thank you!</h4>
          <p>You have successfully joined our subscriber list.</p>
        </div>
      </div> */}
    </div>
  </div>
  </div>

    </React.Fragment>
  )
}
