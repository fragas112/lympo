import React from 'react'
import styles from '../../styles/subscribe.module.scss'


export default function Form() {

  return (
    <React.Fragment>
<div id="mlb2-3612595" >
  <div>
    <div>
      <div>
        <form className={styles.MobileForm} action="https://static.mailerlite.com/webforms/submit/z1g9s4" data-code="z1g9s4" method="post" target="_blank">
        <h1>Some awesome NFT sporting heroes!</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In accumsan neque sed felis vulputate rhoncus. Duis viverra erat at scelerisque pretium. Nunc mattis lacus eget placerat tempor. In rutrum velit vel ex ultricies luctus. Donec a purus ornare, ullamcorper turpis et, hendrerit elit. Proin euismod nunc eget libero convallis finibus.Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
       
          <input aria-label="email" aria-required="true" type="email" className="form-control" data-inputmask="" name="fields[email]" placeholder="Email address" autoComplete="email"/>
          <input aria-label="name" aria-required="true" type="text" className="form-control" data-inputmask="" name="fields[name]" placeholder="ERC20 wallet address" autoComplete="name"/>
          
          <input type="hidden" name="ml-submit" value="1"/>
          <div>
            <button type="submit" className="primary">SUBSCRIBE</button>
          </div>
          <input type="hidden" name="anticsrf" value="true"/>
        </form>
      </div>
      
      {/* <div >
        <div >
          <h4>Thank you!</h4>
          <p>You have successfully joined our subscriber list.</p>
        </div>
      </div> */}
    </div>
  </div>
  </div>

    </React.Fragment>
  )
}
