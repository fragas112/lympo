import { Action, getLanguageProps, EnumActionTypes } from "../types";

export interface languageReducerState {
  [x: string]: string | any;
}

//INIT STATE
export const initialState = {
  lngValue: "en",
  stage: "3rd",
};
//REDUCERS
const getLanguageReducer = (
  state: languageReducerState,
  action: getLanguageProps
) => {
  return {
    ...state,
    lt: action.payload.lt as Object,
    en: action.payload.en as Object,
    ru: action.payload.ru as Object,
  };
};

const changeLanguageReducer = (state: languageReducerState, action: any) => {
  return {
    ...state,
    lngValue: action.payload as string,
  };
};

export const languageReducer = (
  state: languageReducerState = initialState,
  action: Action
) => {
  switch (action.type) {
    case EnumActionTypes.GET_LANGUAGE:
      return getLanguageReducer(state, action);
    case EnumActionTypes.CHANGE_LANGUAGE:
      return changeLanguageReducer(state, action);
    default:
      return state;
  }
};
