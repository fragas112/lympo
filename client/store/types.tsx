export enum EnumActionTypes {
  // LANGUAGE TYPES
  GET_LANGUAGE = "GET_LANGUAGE",
  CHANGE_LANGUAGE = "CHANGE_LANGUAGE",

  // SERVER URL
}
// export let prodURL = "https://supermetalai.lt/backend/"
export let localURL = "http://18.217.193.89/backend"
export interface getLanguageProps {
  type: EnumActionTypes.GET_LANGUAGE;
  payload: {
    lt: Object;
    en: Object;
    ru: Object;
  };
}
export interface changeLanguageProps {
  payload: string;
  type: EnumActionTypes.CHANGE_LANGUAGE;
}

export type Action = getLanguageProps | changeLanguageProps;
