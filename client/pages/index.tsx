import Head from 'next/head';
import React, { useEffect } from 'react';
import HeroBanner from '../components/HeroBanner/HeroBanner';
import HeroBanner2nd from '../components/HeroBanner2/HeroBanner2nd';
import HeroBaner3rd from '../components/HeroBanner3/HeroBanner3rd';
import Main from '../components/Main/Main';
import { useSelector } from "react-redux";
import { StoreState } from '../store/store';

export default function Home() {
   //Language
 const language = useSelector((state: StoreState) => state.language);
  const stage = language?.stage

  useEffect(() => {
    const script = document.createElement('script');
    script.async = true;
    script.src = "https://static.mailerlite.com/js/universal.js";
    document.body.appendChild(script);
    return () => {
      document.body.removeChild(script);
    }
  }, []);
  
  return (
    <React.Fragment>
      <Head>
          <title>Lympo</title>
          <meta name="author" content="Iverta" />
          <meta property="og:title" content="Official Lympo website" />
          <meta property="og:image" content="/LogoLympo.webp" />
          <meta property="og:site_name" content="Lympo" />
      {/* <meta property="og:url" content="http://www.zyq108.lt/" /> */}
          <meta property="og:description" content="Page is for official needs of Lympo" />
      </Head>
      <div id="Home">
        {stage === "1st" ? <HeroBanner /> : stage === "2nd"  ? <HeroBanner2nd/> : <HeroBaner3rd/>}
        <Main/>
      </div>
    </React.Fragment>
  )
}
