
import '../styles/globals.css'
import '../components/FlipCountdown/FlipCountDown.scss'
import { Provider } from "react-redux";
import { useStore } from "../store/store";
import { getLanguage } from "../store/Actions/languageActions";
import React from 'react'
import "../styles/globals.css";
export default function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);

  store.dispatch(getLanguage());

  return (
    <React.Fragment>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </React.Fragment>
  );
}