import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head>
        <link href="https://fonts.googleapis.com/css2?family=Chakra+Petch:wght@300;700&family=Poppins:wght@100&display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Chakra+Petch:wght@300;700&family=Poppins:wght@100;400&display=swap" rel="stylesheet"/>
        </Head>
     
        
        <body>
          <Main />
          <NextScript />
          <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        </body>
      </Html>
    )
  }
}

export default MyDocument